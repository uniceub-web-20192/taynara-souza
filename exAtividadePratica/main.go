package main

import ( "net/http"
		 "time" 
	    // "fmt" 
		 "log"
		 "io/ioutil"
		 "encoding/json"
		 "github.com/gorilla/mux")
		// "gopkg.in/go-playground/validator.v9")


var Users []User = []User{}

var BodyErr string = `{"Status 400":"Invalid Body"}`

var HeaderErr string = `{"Status 400":"Invalid Header"}`

var SuccessMsg string = `{"Status 200":"Successfully Registered User"}`

var LoginSuccess string = `{"Login with Success"}`


type User struct {

	Email string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
	Birthdate string `json:"birthdate" validate:"required"`

}


func main() {
	StartServer()
	//log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/signup", Signup)
	r.HandleFunc("/signin", Signin)


	server := &http.Server{
			//Addr       : "192.168.1.3:8085",
			Addr	   : "192.168.43.9:8085",
			IdleTimeout: duration,
			Handler    : r, 
	}

	//log.Print(server.ListenAndServe())
	log.Fatal(server.ListenAndServe())

}



func Signup(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)

	err := json.Unmarshal(body, &u)

	
	if err != nil {
		res.Write([]byte(BodyErr))
		log.Println(err)
	}

	if u.Email == "" || u.Password == "" || u.Birthdate == "" {
		res.Write([]byte(BodyErr))

	} else {
		Users = append(Users, u)
		res.Write([]byte(SuccessMsg))
	}

	res.Write([]byte(body))
}

func BuscUSer(Users []User,u User) (bool, User){
	for i := range Users {
	    if (Users[i].Email ==u.Email && Users[i].Password ==u.Password) {
			return true, Users[i];
		}
	}
	return false, u;
}

func Signin(res http.ResponseWriter, req *http.Request){

	var u User
	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)
 	ExistsUser, _ := BuscUSer(Users, u)

	if(ChecksMHeader(res, req)){
		if (u.Email == "" ||u.Password == ""){
			res.Header().Set("Status", "400")
			res.Write([]byte(BodyErr))

		}else if (!ExistsUser){
			res.Header().Set("Status", "403")

		}else if (ExistsUser){
			
			res.Write([]byte(LoginSuccess))
		}
	}
}

func ChecksMHeader(res http.ResponseWriter, req *http.Request)(bool){
	m := req.Method 
	h:= req.Header
	expectedHeader := h["Content-Type"]

	if (m != "POST"){
		res.Header().Set("Status", "405")
		return false
	}else if expectedHeader[0] != "application/json"{
		res.Header().Set("Status", "400")
		res.Write([]byte(HeaderErr))
		return false
	}
	return true
}




		
	


