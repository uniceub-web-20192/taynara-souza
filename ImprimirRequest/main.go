package main

import ( "net/http"
		 "time" 
		 "log"
		  "fmt")

func main() {

	StartServer()

}

func morango(w http.ResponseWriter, r *http.Request) {
	
	url := fmt.Sprintf("%v %v", r.Method, r.URL)
	w.Write([]byte(url))
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			//Addr       : "172.22.51.151:8085",
			
			Addr       : "192.168.1.6:8085", 
			IdleTimeout: duration, 
	}

	http.HandleFunc("/", morango)

	log.Print(server.ListenAndServe())
}